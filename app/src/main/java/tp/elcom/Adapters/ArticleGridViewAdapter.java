package tp.elcom.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import tp.elcom.Objects.Article;
import tp.elcom.R;

/**
 * Created by PaFi on 17. 10. 2015.
 */

public class ArticleGridViewAdapter extends ArrayAdapter<Article> {

    Article item;
    ViewHolder holder;
    private Context context;
    private int layoutResourceId;
    private ArrayList<Article> data = new ArrayList<>();
    String euro = "\u20ac";

    public ArticleGridViewAdapter(Context context, int layoutResourceId, ArrayList<Article> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        item = data.get(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
        } else {
            holder = (ViewHolder) row.getTag();
        }

        if (item.getName() != null) {

            GradientDrawable bgShape = (GradientDrawable) row.getBackground();
            bgShape.setColor(Color.parseColor(item.getColor()));

            holder.nameArt = (TextView) row.findViewById(R.id.agName);
            holder.priceArt = (TextView) row.findViewById(R.id.agPrice);
            holder.countArt = (TextView) row.findViewById(R.id.agCount);
            holder.artNameButton = (Button) row.findViewById(R.id.buttonArt);

            holder.nameArt.setText(item.getName());
            holder.countArt.setText(item.getCount()+" "+item.getUnit());
            holder.priceArt.setText("" + item.getPrice() + euro);
        }
        row.setTag(holder);
        return row;
    }

    public void updateCount(Article a) {
        //holder.countArt.setText("" + String.format("%.2f", a.getPrice() * a.getCount())+" €");
        holder.countArt.setText(a.getCount()+" "+a.getUnit());
    }

    static class ViewHolder {
        Button artNameButton;
        TextView nameArt;
        TextView priceArt;
        TextView countArt;
    }

}