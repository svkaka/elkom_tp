package tp.elcom.Adapters;

/**
 * Created by Veronika on 1.11.2015.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import tp.elcom.Objects.Category;
import tp.elcom.R;

public class CategoryGridViewAdapter extends ArrayAdapter<Category> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<Category> data = new ArrayList<>();

    public CategoryGridViewAdapter(Context context, int layoutResourceId, ArrayList<Category> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        Category item = data.get(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
        } else {
            holder = (ViewHolder) row.getTag();
        }

        if (item.getName() != null) {
            GradientDrawable bgShape = (GradientDrawable) row.getBackground();
            bgShape.setColor(Color.parseColor(item.getColor()));

                holder.catNameButton= (Button) row.findViewById(R.id.buttonCat);
            holder.ceName= (TextView) row.findViewById(R.id.cgName);
                holder.ceName.setText(item.getName());
        }
        row.setTag(holder);
        return row;
    }

    static class ViewHolder {
        Button catNameButton;
        TextView ceName;
    }
}