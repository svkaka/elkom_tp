package tp.elcom.Adapters;

/**
 * Created by Veronika on 1.11.2015.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import tp.elcom.Dialogs.EditItemDialog;
import tp.elcom.MainActivity;
import tp.elcom.Objects.Article;
import tp.elcom.Objects.Category;
import tp.elcom.R;

public class EditArticleGridAdapter extends ArrayAdapter<Article> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<Article> data = new ArrayList<>();
    private FragmentManager fm;
    private GridView gridView;
    private EditArticleGridAdapter adapter;
    private boolean isInAll = false;
    String euro = "\u20ac";

    public EditArticleGridAdapter(Context context, int layoutResourceId, ArrayList<Article> data, FragmentManager fm) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.fm=fm;
        this.adapter = this;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;
        final Article item = data.get(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
        } else {
            holder = (ViewHolder) row.getTag();
        }

        if (item.getName() != null) {
            holder.artNameButton= (Button) row.findViewById(R.id.buttonArtEdt);
            holder.artSetButton=(Button) row.findViewById(R.id.buttonArtEdtSets);
            holder.artDelButton= (Button) row.findViewById(R.id.buttonArtEdtDel);
            holder.ageName= (TextView) row.findViewById(R.id.ageName);
            holder.agePrice= (TextView) row.findViewById(R.id.agePrice);
            holder.ageUnit = (TextView) row.findViewById(R.id.ageUnit);

            holder.ageName.setText(item.getName());
            holder.agePrice.setText("" + item.getPrice() + euro);
            holder.ageUnit.setText(item.getUnit());


            GradientDrawable bgShape = (GradientDrawable) row.getBackground();
            bgShape.setColor(Color.parseColor(item.getColor()));


                holder.artSetButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!((Button) v).getText().equals("All")) {
                            EditItemDialog dialog2 = new EditItemDialog();
                            Bundle args = new Bundle();
                            args.putString("item", item.getClass().toString());
                            dialog2.setArguments(args);
                            dialog2.setGridView(gridView);
                            dialog2.setAdapter(adapter);
                            dialog2.setItem(item);
                            dialog2.show(fm, "edit article");
                        }
                    }
                });

            holder.artDelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.dataSource.deleteArticle(item);
                    System.out.println(item.getDbsID());
                                        //MainActivity.allArticles.remove(position);
                    Iterator<Category> iterator = MainActivity.categories.iterator();
                    Category cat = null;
                    iterator.next().getArticles().remove(item);
                    while (iterator.hasNext()) {
                        cat = iterator.next();
                        if(cat.getName().equals(item.getCategory())) {
                            cat.getArticles().remove(item);
                            break;
                        }
                    }
                    MainActivity.allArticles.remove(item);
                    //System.out.println(MainActivity.categories);
                    //if (isInAll) adapter = new EditArticleGridAdapter(getContext(), R.layout.article_grid_edit, (ArrayList<Article>) MainActivity.allArticles,fm);
                    //    else if (cat != null) adapter = new EditArticleGridAdapter(getContext(), R.layout.article_grid_edit, (ArrayList<Article>) cat.getArticles(),fm);
                    //if (gridView != null) {
                    notifyDataSetChanged();
                        //gridView.setAdapter(adapter);
                        //((EditArticleGridAdapter) gridView.getAdapter()).notifyDataSetChanged();
                    //}
                }
            });

            }
            row.setTag(holder);


        return row;
    }

    public void setGridView(GridView gridView) {
        this.gridView = gridView;
    }

    public void setAdapter(EditArticleGridAdapter adapter) {
        this.adapter = adapter;
    }

    public void setIsInAll(boolean isInAll) {
        this.isInAll = isInAll;
    }

    static class ViewHolder {
        Button artNameButton;
        Button artDelButton;
        Button artSetButton;
        TextView agePrice;
        TextView ageName;
        TextView ageUnit;
    }
}