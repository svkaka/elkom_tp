package tp.elcom.Adapters;

/**
 * Created by Veronika on 1.11.2015.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import tp.elcom.Dialogs.EditItemDialog;
import tp.elcom.MainActivity;
import tp.elcom.Objects.Article;
import tp.elcom.Objects.Category;
import tp.elcom.R;

public class EditCategoryGridAdapter extends ArrayAdapter<Category> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<Category> data = new ArrayList<>();
    private FragmentManager fm;
    private GridView gridView;
    private EditCategoryGridAdapter adapter;

    public EditCategoryGridAdapter(Context context, int layoutResourceId, ArrayList<Category> data,FragmentManager fm) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.fm=fm;
        this.adapter = this;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        final Category item = data.get(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
        } else {
            holder = (ViewHolder) row.getTag();
        }


            if (item.getName() != null) {
                GradientDrawable bgShape = (GradientDrawable) row.getBackground();
                bgShape.setColor(Color.parseColor(item.getColor()));

                holder.catNameButton= (Button) row.findViewById(R.id.buttonCatEdt);
                holder.catSetButton=(Button) row.findViewById(R.id.buttonCatEdtSets);
                holder.catDelButton= (Button) row.findViewById(R.id.buttonCatEdtDel);
                holder.cgeName= (TextView) row.findViewById(R.id.cgeName);

                holder.cgeName.setText(item.getName());
                holder.catSetButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!((Button) v).getText().equals("all")) {
                            int pos = gridView.getFirstVisiblePosition();
                            EditItemDialog dialog2 = new EditItemDialog();
                            Bundle args = new Bundle();
                            args.putString("item", item.getClass().toString());
                            dialog2.setArguments(args);
                            dialog2.setGridView(gridView);
                            dialog2.setAdapter(adapter);
                            dialog2.setItem(item);
                            dialog2.setPosition(pos);
                            dialog2.show(fm, "edit category");
                        }
                    }
                });
                if(item.getIdentifier() == 0) holder.catDelButton.setVisibility(View.GONE);
                holder.catDelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //if(position != 0) {
                            Iterator<Article> iterator = item.getArticles().iterator();
                            while (iterator.hasNext()) {
                                Article a = iterator.next();
                                MainActivity.dataSource.deleteArticle(a);
                                MainActivity.allArticles.remove(a);
                                //MainActivity.categories.get(0).getArticles().remove(iterator.next());
                            }
                        MainActivity.categories.remove(item);
                        MainActivity.dataSource.deleteCategory(item);
                        //((EditCategoryGridAdapter)gridView.getAdapter()).

                        //}
                        notifyDataSetChanged();
                    }
                });

            }
            row.setTag(holder);


        return row;
    }

    public void setGridView(GridView gridView) {
        this.gridView = gridView;
    }

    static class ViewHolder {
        Button catNameButton;
        Button catSetButton;
        Button catDelButton;
        TextView cgeName;
    }
}