package tp.elcom.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import tp.elcom.Objects.Article;
import tp.elcom.R;


/**
 * Created by PaFi on 17. 10. 2015.
 */


public class ReceiptListViewAdapter extends ArrayAdapter<Article> {
    private Context context;
    private int layoutResourceId;
    private ArrayList<Article> data = new ArrayList<>();
    public int pos;
    String euro = "\u20ac";
    public ReceiptListViewAdapter(Context context, int layoutResourceId, ArrayList<Article> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        pos = position;
        Article item = data.get(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
        } else {
            holder = (ViewHolder) row.getTag();
        }
         if (item != null) {
                holder.artName = (TextView) row.findViewById(R.id.rcptArticleTxt);
                holder.artCount = (TextView) row.findViewById(R.id.rcptCountTxd);
                holder.artTotPrice = (TextView) row.findViewById(R.id.rcptTotalPriceTxt);
                holder.artRemove= (Button) row.findViewById(R.id.rcptRemove);
                holder.artName.setText(item.getName());
                holder.artCount.setText(Integer.toString(item.getCount())+" "+item.getUnit());
             holder.artTotPrice.setText(String.format("%.2f " + euro, item.getPrice() * item.getCount()));
             if (position % 2==1){
                 row.setBackgroundColor(Color.LTGRAY);
             }
            }
            row.setTag(holder);

        return row;
    }

    static class ViewHolder {
        TextView artName;
        TextView artCount;
        Button artRemove;
        TextView artTotPrice;
    }
}

