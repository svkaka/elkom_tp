package tp.elcom.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import java.util.ArrayList;

import tp.elcom.Fragments.CategoryFragment;
import tp.elcom.Fragments.CheckFragment;
import tp.elcom.Fragments.EditFragment;

/**
 * Created by Lenovo on 1. 11. 2015.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> tabs;
    private CheckFragment f;

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        switch (position) {
            case 0:
                return CategoryFragment.newInstance();
            case 1:
                f = CheckFragment.newInstance();
                //f.updateFragment();
                return getF();
            case 2:
                return EditFragment.newInstance();
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    public void setTabs(ArrayList<String> myTabs) {
        this.tabs = myTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return tabs.get(0);
            case 1:
                return tabs.get(1);
            case 2:
                return tabs.get(2);
        }
        return null;
    }

    public CheckFragment getF() {
        return f;
    }
}
