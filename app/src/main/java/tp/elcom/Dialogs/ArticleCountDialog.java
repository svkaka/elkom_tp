package tp.elcom.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import tp.elcom.Adapters.ArticleGridViewAdapter;
import tp.elcom.Fragments.CategoryFragment;
import tp.elcom.MainActivity;
import tp.elcom.Objects.Article;
import tp.elcom.R;

/**
 * Created by Veronika on 23. 12. 2015.
 */
public class ArticleCountDialog extends DialogFragment implements DialogInterface.OnClickListener, NumberPicker.OnValueChangeListener  {

    private AlertDialog.Builder builder;
    private ArticleGridViewAdapter adapter;
    private View view;
    private NumberPicker counter;
    private TextView unit;
    private Article item;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater i = getActivity().getLayoutInflater();
        view = i.inflate(R.layout.unit_dialog, null);
        unit = (TextView)view.findViewById(R.id.unit);
        unit.setText(item.getUnit());
        counter = (NumberPicker)view.findViewById(R.id.numberPicker);
        counter.clearFocus();
        counter.setMinValue(0);
        counter.setMaxValue(10000);
        counter.setWrapSelectorWheel(false);
        counter.setOnValueChangedListener(this);
        counter.setValue(item.getCount());

        builder.setView(view);
        builder.setTitle(getString(R.string.countDialogMessage)+": "+item.getName())
                .setPositiveButton(R.string.ok, this)
                .setNegativeButton(R.string.cancel, this);
        builder.setCancelable(false);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case DialogInterface.BUTTON_POSITIVE:
                if(item != null) {
                    counter.clearFocus();
                    item.setCount(counter.getValue());
                    adapter.updateCount(item);
                    adapter.notifyDataSetChanged();

                    boolean isInReceipt = false;
                    for (int j = 0; j < MainActivity.receiptItems.size(); j++) {
                        if ((MainActivity.receiptItems.get(j).getIdentifier().equals(item.getIdentifier()))) {
                            isInReceipt = true;
                        }
                    }
                    if (!isInReceipt) {
                        if(item.getCount() > 0) MainActivity.receiptItems.add(item);
                    }
                    if(item.getCount() == 0) MainActivity.receiptItems.remove(item);
                }
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.cancel();
                break;
        }
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        counter.clearFocus();
    }

    public void setItem(Article item) {
        this.item = item;
    }

    public void setAdapter(ArticleGridViewAdapter adapter) {
        this.adapter = adapter;
    }
}
