package tp.elcom.Dialogs;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import tp.elcom.Adapters.EditArticleGridAdapter;
import tp.elcom.Adapters.SpecialSpinnerAdapter;
import tp.elcom.MainActivity;
import tp.elcom.Objects.Article;
import tp.elcom.Objects.Category;
import tp.elcom.Objects.Database;
import tp.elcom.Objects.Verifier;
import tp.elcom.R;

/**
 * Created by PaFi on 16. 10. 2015.
 */
public class CreateItemDialog extends DialogFragment implements DialogInterface.OnClickListener  {

    private AlertDialog.Builder builder;
    private RadioButton checkedRadioButton;
    private Spinner spinner;
    private Category category = new Category();
    private View view;
    private ArrayList<String> categoriesList;
    private ArrayAdapter<String> articleDataAdapter;
    private SpecialSpinnerAdapter articleSpinnerAdapter;
    private ImageView showColour;
    private EditText price, id, name;
    private Spinner unit;
    private boolean isArticle;
    private GridView gridView;
    private Button customColour, randomColour;
    private TextView colour;
    private String defaultColour;
    public static CreateItemDialog instance;

    public CreateItemDialog() {
        instance = this;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater i = getActivity().getLayoutInflater();
        view = i.inflate(R.layout.add_item_dialog, null);
        //spinnerText = (TextView)view.findViewById(R.id.textView3);
        //spinnerText.setText("Category");
        RadioButton radioButton1 = (RadioButton)view.findViewById(R.id.articleRadioButton);
        RadioButton radioButton2 = (RadioButton)view.findViewById(R.id.categoryRadioButton);
        price = (EditText)view.findViewById(R.id.priceEditText1);
        id = (EditText)view.findViewById(R.id.idEditText);
        unit = (Spinner)view.findViewById(R.id.unitSpinner);
        name = (EditText)view.findViewById(R.id.nameEditText1);
        colour = (TextView)view.findViewById(R.id.showColourText);
        showColour = (ImageView)view.findViewById(R.id.showColourView);
        radioButton1.setChecked(true);
        isArticle = true;
        checkedRadioButton = radioButton1;
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final RadioButton mBtnRadio = (RadioButton) v;
                // select only one radio button at any given time
                if (checkedRadioButton != null) {
                    checkedRadioButton.setChecked(false);
                }
                mBtnRadio.setChecked(true);
                checkedRadioButton = mBtnRadio;
                if (v.getId() == R.id.categoryRadioButton) {
                    //spinnerText.setText(R.string.colour);
                    //priceTextView.setVisibility(View.GONE);
                    price.setVisibility(View.GONE);
                    colour.setVisibility(View.VISIBLE);
                    showColour.setVisibility(View.VISIBLE);
                    customColour.setVisibility(View.VISIBLE);
                    randomColour.setVisibility(View.VISIBLE);
                    //idTextView.setVisibility(View.GONE);
                    id.setVisibility(View.GONE);
                    unit.setVisibility(View.GONE);
                    //spinner.setPrompt(getString(R.string.colour));
                    //spinner.setAdapter(categorySpinnerAdapter);
                    spinner.setVisibility(View.GONE);
                    defaultColour = new Verifier().getColor();
                    showColour.setBackgroundColor(Color.parseColor(defaultColour));
                    category.setColor(defaultColour);
                    isArticle = false;
                    builder.setView(view);
                    builder.create();
                }
                else {
                    //spinnerText.setText(R.string.addCategory);
                    colour.setVisibility(View.GONE);
                    showColour.setVisibility(View.GONE);
                    customColour.setVisibility(View.GONE);
                    randomColour.setVisibility(View.GONE);
                    price.setVisibility(View.VISIBLE);
                    unit.setVisibility(View.VISIBLE);
                    /*String[] unitList = getResources().getStringArray(R.array.units);
                    ArrayAdapter unitAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, unitList);
                    SpecialSpinnerAdapter unitSpinnerAdapter = new SpecialSpinnerAdapter(unitAdapter, R.layout.spinner_row, getContext());
                    unit.setAdapter(unitSpinnerAdapter);
                    unit.setPrompt(getString(R.string.insert_unit));*/
                    id.setVisibility(View.VISIBLE);
                    spinner.setVisibility(View.VISIBLE);
                    spinner.setPrompt(getString(R.string.addCategory));
                    spinner.setAdapter(articleSpinnerAdapter);
                    isArticle = true;
                    // set the view displayed by builder
                    builder.setView(view);
                    builder.create();
                }
            }
        };
        radioButton1.setOnClickListener(listener);
        radioButton2.setOnClickListener(listener);

        spinner = (Spinner)view.findViewById(R.id.categorySpinner);
        //spinnerText.invalidate();
        // create list of category names
        //System.out.println("kategorie: "+MainActivity.categories.size());
        categoriesList = new ArrayList<>();
        for(Category c : MainActivity.categories) {
            //System.out.println(c.getIdentifier());
            if(c.getIdentifier() != 0) categoriesList.add(c.getName());
        }
        // create adapter for spinner items
        articleDataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categoriesList);
        articleDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        articleSpinnerAdapter = new SpecialSpinnerAdapter(articleDataAdapter, R.layout.spinner_row_category, R.layout.spinner_row, getContext());
        spinner.setAdapter(articleSpinnerAdapter);

        String[] unitList = getResources().getStringArray(R.array.units);
        ArrayAdapter unitAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, unitList);
        SpecialSpinnerAdapter unitSpinnerAdapter = new SpecialSpinnerAdapter(unitAdapter, R.layout.spinner_row_unit,
                R.layout.spinner_row, getContext());
        unit.setPrompt(getString(R.string.insert_unit));
        unit.setAdapter(unitSpinnerAdapter);

        colour.setVisibility(View.GONE);
        showColour.setVisibility(View.GONE);
        customColour = (Button)view.findViewById(R.id.customColour1);
        customColour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomColourDialog dialog = new CustomColourDialog();
                dialog.setItem(category);
                dialog.setIsInEditMode(false);
                dialog.show(getFragmentManager(), "custom colour");
            }
        });
        customColour.setVisibility(View.GONE);
        randomColour = (Button)view.findViewById(R.id.randomColour1);
        randomColour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setColor(new Verifier().getColor());
                //System.out.println("FARBA "+Color.parseColor(category.getColor()));
                showColour.setBackgroundColor(Color.parseColor(category.getColor()));
            }
        });
        randomColour.setVisibility(View.GONE);
        // set the view displayed by builder
        builder.setView(view)
        //builder.setTitle(R.string.addItemDialogMessage)
                .setPositiveButton(R.string.ok, this)
                .setNegativeButton(R.string.cancel, this);
        builder.setCancelable(true);

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:

                if (isArticle) {
                    if (spinner.getSelectedItem()!= null) {
                        String idd = id.getText().toString() + "";
                        Verifier verifier = new Verifier(idd, name.getText().toString(),
                                price.getText().toString(), unit.getSelectedItem().toString(), spinner.getSelectedItem().toString());
                        Object o = verifier.verify();
                        if (o != null) {
                            MainActivity.allArticles.add((Article) o);
                            verifier.assignCategories();
                            ContentValues values=new ContentValues();
                            values.put(Database.ARTICLE_NAME, ((Article) o).getName());
                            values.put(Database.ARTICLE_PRICE, ((Article) o).getPrice());
                            values.put(Database.ARTICLE_UNIT, ((Article) o).getUnit());
                            values.put(Database.ARTICLE_IDENTIFIER, ((Article) o).getIdentifier());
                            Iterator<Category> iterator = MainActivity.categories.iterator();
                            Category c = null;
                            while (iterator.hasNext()){
                                c = iterator.next();
                                if (c.equals(((Article) o).getCategory())) break;
                            }
                            values.put(Database.ARTICLE_CAT, c.getIdentifier());
                            MainActivity.dataSource.insertArticle(values);

                            c = null;
                            for(Category cat : MainActivity.categories) {
                                if (cat.getName().equalsIgnoreCase(((Article) o).getCategory())) c = cat;
                            }
                            List<Article> articles = c.getArticles();
                            EditArticleGridAdapter editArticleGridAdapter = new EditArticleGridAdapter(getContext(), R.layout.article_grid_edit, (ArrayList<Article>) articles,getFragmentManager());
                            gridView.setAdapter(editArticleGridAdapter);
                        }
                        else Toast.makeText(getContext(), R.string.toast1, Toast.LENGTH_SHORT).show();
                    }
                    else Toast.makeText(getContext(), R.string.toast1, Toast.LENGTH_SHORT).show();
                    }
                else {
                    //if (spinner.getSelectedItem()!= null) {
                        Verifier verifier = new Verifier(name.getText().toString(), ""); //spinner.getSelectedItem().toString());
                        Object o = verifier.verify();
                        if (o != null) {
                            ((Category)o).setColor(category.getColor());
                            MainActivity.categories.add((Category) o);

                            ContentValues values = new ContentValues();
                            values.put(Database.CATEGORY_NAME, ((Category) o).getName());
                            values.put(Database.CATEGORY_COLOUR, ((Category) o).getColor());
                            MainActivity.dataSource.insertCategory(values);

                        }
                            else Toast.makeText(getContext(), R.string.toast2, Toast.LENGTH_SHORT).show();
                    //}
                    //else Toast.makeText(getContext(), "Category not created.", Toast.LENGTH_SHORT).show();
                }
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.cancel();
                break;
        }
    }

    public void setGridView(GridView gridView) {
        this.gridView = gridView;
    }

    public void updateColourView(String colour) {
        showColour.setBackgroundColor(Color.parseColor(colour));
    }
    /*public void setItem(Object item) {
        this.item = item;
    }*/
}
