package tp.elcom.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import tp.elcom.Objects.Article;
import tp.elcom.Objects.Category;
import tp.elcom.R;

/**
 * Created by Veronika on 21. 12. 2015.
 */
public class CustomColourDialog extends DialogFragment implements DialogInterface.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private AlertDialog.Builder builder;
    private View view;
    private SeekBar r, g, b;
    private int valueR, valueG, valueB;
    private TextView hexaColour;
    private ImageView canvas;
    private Category item;
    private boolean isInEditMode;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater i = getActivity().getLayoutInflater();
        view = i.inflate(R.layout.custom_colour_dialog, null);
        hexaColour = (TextView)view.findViewById(R.id.hexaColour);
        hexaColour.setText("#"+intToHexa(valueR)+intToHexa(valueG)+intToHexa(valueB));
        canvas = (ImageView)view.findViewById(R.id.customColourView);
        canvas.setColorFilter(Color.parseColor("#000000"));
        r = (SeekBar)view.findViewById(R.id.seekBarR);
        g = (SeekBar)view.findViewById(R.id.seekBarG);
        b = (SeekBar)view.findViewById(R.id.seekBarB);
        r.setId(0);
        g.setId(Integer.parseInt("1"));
        b.setId(Integer.parseInt("2"));
        r.setOnSeekBarChangeListener(this);
        g.setOnSeekBarChangeListener(this);
        b.setOnSeekBarChangeListener(this);

        builder.setView(view);
        builder.setPositiveButton(R.string.ok, this)
                .setNegativeButton(R.string.cancel, this);
        builder.setCancelable(false);

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case DialogInterface.BUTTON_POSITIVE:
                item.setColor("#" + intToHexa(valueR) + intToHexa(valueG) + intToHexa(valueB));
                for(Article a : item.getArticles()) a.setColor("#"+intToHexa(valueR)+intToHexa(valueG)+intToHexa(valueB));
                if (isInEditMode) EditItemDialog.instance.updateColourView("#" + intToHexa(valueR) + intToHexa(valueG) + intToHexa(valueB));
                else CreateItemDialog.instance.updateColourView("#" + intToHexa(valueR) + intToHexa(valueG) + intToHexa(valueB));
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.cancel();
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case 0:
                valueR = progress;//int finalColor = Color.argb();
                hexaColour.setText("#" + intToHexa(valueR) + intToHexa(valueG) + intToHexa(valueB));
                canvas.setBackgroundColor(Color.parseColor("#" + intToHexa(valueR) + intToHexa(valueG) + intToHexa(valueB)));
                break;
            case 1:
                valueG = progress;
                hexaColour.setText("#"+intToHexa(valueR)+intToHexa(valueG)+intToHexa(valueB));
                canvas.setBackgroundColor(Color.parseColor("#" + intToHexa(valueR) + intToHexa(valueG) + intToHexa(valueB)));
                break;
            case 2:
                valueB = progress;
                hexaColour.setText("#" + intToHexa(valueR)+ intToHexa(valueG)+ intToHexa(valueB));
                canvas.setBackgroundColor(Color.parseColor("#" + intToHexa(valueR) + intToHexa(valueG) + intToHexa(valueB)));
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public String intToHexa(int number) {
        String s = Integer.toHexString(number).toUpperCase();
        if(number < 16) s = "0"+s;
        return s;
    }

    public void setItem(Category item) {
        this.item = item;
    }

    public void setIsInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;
    }
}
