package tp.elcom.Dialogs;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Iterator;
import java.util.List;

import tp.elcom.Adapters.SpecialSpinnerAdapter;
import tp.elcom.MainActivity;
import tp.elcom.Objects.Article;
import tp.elcom.Objects.Category;
import tp.elcom.Objects.Database;
import tp.elcom.Objects.Verifier;
import tp.elcom.R;

/**
 * Created by PaFi on 16. 10. 2015.
 */
public class EditItemDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private AlertDialog.Builder builder;
    private View view;
    private EditText name, price;
    private Spinner unit;
    private Object item;
    private String s;
    private Button customColour, randomColour;
    private GridView gridView;
    private ArrayAdapter adapter;
    String[] unitList;
    private TextView priceLabel, colourLabel;
    private ImageView showColour;
    public static EditItemDialog instance;
    private int position;

    public EditItemDialog() {
        instance = this;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        s = getArguments().getString("item");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater i = getActivity().getLayoutInflater();
        view = i.inflate(R.layout.edit_item_dialog, null);
        //unitLabel = (TextView) view.findViewById(R.id.unitTextView);
        unit = (Spinner) view.findViewById(R.id.unitSpinnerEdit);
        unitList = getResources().getStringArray(R.array.units);
        ArrayAdapter unitAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, unitList);
        SpecialSpinnerAdapter unitSpinnerAdapter = new SpecialSpinnerAdapter(unitAdapter, R.layout.spinner_row, getContext());
        unit.setAdapter(unitSpinnerAdapter);
        //updateUnitSpinner();

        colourLabel = (TextView)view.findViewById(R.id.colourTextView);
        showColour = (ImageView)view.findViewById(R.id.showColourViewInEdit);
        customColour = (Button)view.findViewById(R.id.customColour);
        customColour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomColourDialog dialog = new CustomColourDialog();
                dialog.setItem((Category) item);
                dialog.setIsInEditMode(true);
                dialog.show(getFragmentManager(), "custom colour");
            }
        });

        randomColour = (Button)view.findViewById(R.id.randomColour);
        randomColour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Category) item).setColor(new Verifier().getColor());
                Iterator<Article> iterator = ((Category) item).getArticles().iterator();
                while (iterator.hasNext()) {
                    iterator.next().setColor(((Category) item).getColor());
                }
                showColour.setBackgroundColor(Color.parseColor(((Category) item).getColor()));
            }
        });
        name = (EditText) view.findViewById(R.id.nameEditText);
        price = (EditText) view.findViewById(R.id.priceEditText);
        priceLabel = (TextView) view.findViewById(R.id.priceTextView);
        //spinner = (Spinner)view.findViewById(R.id.categoryColorSpinner);
        if(s.contains("Category")) {
            if (((Category) item).getIdentifier() == 0) {
                name.setHint(((Category) item).getName());
                name.setKeyListener(null);
            }
                else name.setText(((Category) item).getName());
            priceLabel.setVisibility(View.GONE);
            price.setVisibility(View.GONE);
            //unitLabel.setVisibility(View.GONE);
            unit.setVisibility(View.GONE);
            showColour.setBackgroundColor(Color.parseColor(((Category) item).getColor()));
            //setUpSpinner();
        }
        else if(s.contains("Article")) {
            showColour.setVisibility(View.GONE);
            colourLabel.setVisibility(View.GONE);
            customColour.setVisibility(View.GONE);
            randomColour.setVisibility(View.GONE);
            name.setText(((Article) item).getName());
            price.setText(String.valueOf(((Article) item).getPrice()));
            updateUnitSpinner();
            //unit.setText(((Article) item).getUnit());
        }

        builder.setView(view);
        builder.setTitle(R.string.editItemDialogMessage)
                .setPositiveButton(R.string.ok, this)
                .setNegativeButton(R.string.cancel, this);
        builder.setCancelable(false);


        // Create the AlertDialog object and return it
        return builder.create();
    }

    /*private void setUpSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.colours_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        colours = Arrays.asList(getResources().getStringArray(R.array.colours_array));
        priceLabel.setText(R.string.colour);
        updateUnitSpinner();
    }*/

    private void updateUnitSpinner() {
        if(item instanceof Article) {
        for(int n = 0; n < unitList.length; n++) {
            if (unitList[n].equals(((Article) item).getUnit())) {
                unit.clearFocus();
                unit.setSelection(n + 1, true);
                //System.out.println(unitList[n].equalsIgnoreCase(((Article) item).getUnit()) + " " + n);
                }
            //System.out.println(unitList[n]+" - "+(((Article) item).getUnit()));
        }
        }
    }

    /*private String findUnit(String u) {
        Set set = colourHash.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if(entry.getValue().equals(u)) return entry.getKey().toString();
        }
        return "ks";
    }*/


    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case DialogInterface.BUTTON_POSITIVE:
                if (item instanceof Category) {
                        if(((Category) item).getIdentifier() != 0) {
                            if(!name.getText().toString().equals("")) ((Category) item).setName(name.getText().toString());
                        }
                    ContentValues contentValues=new ContentValues();
                    contentValues.put(Database.CATEGORY_NAME, name.getText().toString());
                    contentValues.put(Database.CATEGORY_COLOUR, ((Category) item).getColor());
                    MainActivity.dataSource.updateCategory(contentValues, Database.CATEGORY_ID + " = " + ((Category) item).getIdentifier());
                        }
                        else if (item instanceof Article) {
                        if(!name.getText().toString().equals("")) {
                            ((Article) item).setName(name.getText().toString());
                        }
                        if(!price.getText().toString().equals(""))
                            ((Article) item).setPrice(Float.parseFloat(price.getText().toString()));
                        /*if(!unit.getText().toString().equals(""))
                            ((Article) item).setUnit(unit.getText().toString());*/
                    if(unit.getSelectedItem() != null)((Article) item).setUnit(unit.getSelectedItem().toString());

                    ContentValues contentValues=new ContentValues();
                    contentValues.put(Database.ARTICLE_NAME,name.getText().toString());
                    contentValues.put(Database.ARTICLE_PRICE, price.getText().toString());
                    contentValues.put(Database.ARTICLE_UNIT, unit.getSelectedItem().toString());
                    MainActivity.dataSource.updateArticle(contentValues, Database.ARTICLE_ID +" = " +((Article) item).getDbsID());
                    }
                adapter.notifyDataSetChanged();
                if(gridView != null) {
                    gridView.setAdapter(adapter);
                    gridView.smoothScrollToPosition(position);
                    gridView.setSelection(position);
                }
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                dialog.cancel();
                break;
        }
    }

    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }

    public void setGridView(GridView gridView) {
        this.gridView = gridView;
    }

    public void setAdapter(ArrayAdapter adapter) {
        this.adapter = adapter;
    }

    public void updateColourView(String colour) {
        showColour.setBackgroundColor(Color.parseColor(colour));
    }

    public void setPosition(int pos) {
        this.position = pos;
    }
}
