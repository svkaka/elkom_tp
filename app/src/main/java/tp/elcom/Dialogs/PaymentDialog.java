package tp.elcom.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import tp.elcom.R;

/**
 * Created by PaFi on 17. 10. 2015.
 */
public class PaymentDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private AlertDialog.Builder builder;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.paymentDialogMessage)
                .setPositiveButton(R.string.action_PayCard, this)
                .setNegativeButton(R.string.action_PayCash, this);
        builder.setCancelable(true);

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case DialogInterface.BUTTON_NEGATIVE:
                Toast.makeText(getContext(),"Pay by Card chosen.", Toast.LENGTH_SHORT).show();
                break;
            case DialogInterface.BUTTON_POSITIVE:
                Toast.makeText(getContext(), "Pay by Cash chosen.", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}

