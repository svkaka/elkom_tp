package tp.elcom.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import tp.elcom.Fragments.CheckFragment;
import tp.elcom.MainActivity;
import tp.elcom.R;

/**
 * Created by PaFi on 19. 1. 2016.
 */
public class PriceDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private AlertDialog.Builder builder;
    private View view;
    private EditText received;
    private TextView priceLabel, iny, dalsi;
    private Switch aSwitch;
    private float payed;
    private float dal;
    private boolean methodChosen;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater i = getActivity().getLayoutInflater();
        view = i.inflate(R.layout.dialogprice, null);

        received = (EditText) view.findViewById(R.id.editText);
        priceLabel = (TextView) view.findViewById(R.id.textView10);
        iny = (TextView) view.findViewById(R.id.textView3);
        dalsi = (TextView) view.findViewById(R.id.textView14);
        aSwitch = (Switch) view.findViewById(R.id.switch1);

        received.setVisibility(View.GONE);
        priceLabel.setVisibility(View.GONE);
        iny.setVisibility(View.GONE);
        dalsi.setVisibility(View.GONE);

        String checkFragmentTag = ((MainActivity) getActivity()).getCheckFragmentTag();
        CheckFragment fragment = (CheckFragment) getFragmentManager().findFragmentByTag(checkFragmentTag);
        fragment.setMethod(aSwitch.isChecked());

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    received.setVisibility(View.VISIBLE);
                    priceLabel.setVisibility(View.VISIBLE);
                    iny.setVisibility(View.VISIBLE);
                    dalsi.setVisibility(View.VISIBLE);
                    String checkFragmentTag = ((MainActivity) getActivity()).getCheckFragmentTag();
                    CheckFragment fragment = (CheckFragment) getFragmentManager().findFragmentByTag(checkFragmentTag);
                    fragment.setMethod(isChecked);

                } else {
                    received.setVisibility(View.GONE);
                    priceLabel.setVisibility(View.GONE);
                    iny.setVisibility(View.GONE);
                    dalsi.setVisibility(View.GONE);
                    String checkFragmentTag = ((MainActivity) getActivity()).getCheckFragmentTag();
                    CheckFragment fragment = (CheckFragment) getFragmentManager().findFragmentByTag(checkFragmentTag);
                    fragment.setMethod(isChecked);
                }
            }
        });
        String checkFragmentTag1 = ((MainActivity) getActivity()).getCheckFragmentTag();
        CheckFragment fragment1 = (CheckFragment) getFragmentManager().findFragmentByTag(checkFragmentTag1);
        payed = fragment1.getToPay();

        received.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!received.getText().toString().equals("")) {
                    float vydaj = Float.parseFloat(received.getText().toString());
                    vydaj -= payed;
                    priceLabel.setText(String.format("%.2f", vydaj));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        builder.setView(view);
        builder.setPositiveButton(R.string.ok, this)
                .setNegativeButton(R.string.cancel, this)
                .setCancelable(true);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.cancel();
                break;
            case DialogInterface.BUTTON_POSITIVE:

                String checkFragmentTag = ((MainActivity) getActivity()).getCheckFragmentTag();
                CheckFragment fragment = (CheckFragment) getFragmentManager().findFragmentByTag(checkFragmentTag);

                try {
                    dal = Float.parseFloat(received.getText().toString());
                } catch (NumberFormatException ex) {
                    dal = 0.0f;
                }

                if (aSwitch.isChecked()) {

                    if (dal >= fragment.getToPay()) {
                        fragment.pay(dal);
                    } else Toast.makeText(getContext(), R.string.askjd, Toast.LENGTH_SHORT).show();

                } else {
                    if (fragment.getToPay() >= fragment.getToPay()) {
                        fragment.pay(fragment.getToPay());
                    } else Toast.makeText(getContext(), R.string.askjd, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    public boolean getMethodChosen() {
        return methodChosen;
    }
}
