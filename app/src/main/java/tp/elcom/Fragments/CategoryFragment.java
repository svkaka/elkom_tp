package tp.elcom.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.SoundEffectConstants;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import tp.elcom.Adapters.ArticleGridViewAdapter;
import tp.elcom.Adapters.CategoryGridViewAdapter;
import tp.elcom.Dialogs.ArticleCountDialog;
import tp.elcom.MainActivity;
import tp.elcom.Objects.Article;
import tp.elcom.Objects.Category;
import tp.elcom.Objects.Hunter;
import tp.elcom.Objects.Sorter;
import tp.elcom.R;
import tp.elcom.ToggleButtonGroupTableLayout;

/**
 * Created by PaFi on 16. 10. 2015.
 */

public class CategoryFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    GridView gridView;
    private CategoryGridViewAdapter adapter;
    private ArticleGridViewAdapter adapter2;
    private List<Article> articles = new ArrayList<>();
    OnArticleSelectedListener mListener;
    private SearchView searchView;
    private List<Article> searchTemp = new ArrayList<>();
    private ImageButton sortDesc, sortAsc;
    ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
    private final int duration = 1;
    private final int sampleRate = 8000;
    private final int numSamples = duration * sampleRate;
    private final double sample[] = new double[numSamples];
    private final double freqOfTone = 440;
    private final byte generatedSnd[] = new byte[2 * numSamples];
    Handler handler = new Handler();

    public void updateGridView() {
        adapter.notifyDataSetChanged();
        adapter2.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            Activity a = (Activity) context;
            try {
                mListener = (OnArticleSelectedListener) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(a.toString() + " must implement OnArticleSelectedListener");
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (gridView.getAdapter() == adapter2) {
            ArticleCountDialog dialog = new ArticleCountDialog();
            dialog.setItem(articles.get(position));
            dialog.setAdapter(adapter2);
            dialog.show(getFragmentManager(), "set count");
        }
        return true;
    }

    public interface OnArticleSelectedListener {
        void onArticleSelected(List<Article> a);
    }

    public static CategoryFragment newInstance() {
        return new CategoryFragment();
    }

    public CategoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        String tag = getTag();
        ((MainActivity)getActivity()).setCategoryFragmentTag(tag);
        gridView = (GridView) rootView.findViewById(R.id.gridView);
        adapter = new CategoryGridViewAdapter(getContext(), R.layout.category_grid, (ArrayList<Category>) MainActivity.categories);
        searchView = (SearchView) rootView.findViewById(R.id.searchField);
        sortDesc = (ImageButton) rootView.findViewById(R.id.buttonSortDesc);
        sortAsc = (ImageButton) rootView.findViewById(R.id.buttonSortAsc);

        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this);
        adapter.notifyDataSetChanged();


        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (gridView.getAdapter() instanceof ArticleGridViewAdapter) {
                        gridView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        return true;
                    }
                }
                return false;
            }
        });

        sortDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getActivity(), sortDesc);
                popup.getMenuInflater().inflate(R.menu.popup1, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals(getString(R.string.sortByName))) sortNow(0, "d");
                        else sortNow(1, "d");
                        return true;
                    }
                });
                popup.show();
            }
        });

        sortAsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getActivity(), sortDesc);
                popup.getMenuInflater().inflate(R.menu.popup2, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals(getString(R.string.sortByName))) sortNow(0, "a");
                        else sortNow(1, "a");
                        return true;
                    }
                });
                popup.show();
            }
        });

        //System.out.println(searchView.getQuery());
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                onSearch(s);
                return false;
            }
        });

        return rootView;
    }

    private void sortNow(int which,String s) {
        Sorter sorter=new Sorter();
        MainActivity.allArticles = sorter.sort((ArrayList) MainActivity.allArticles, s, which);
        adapter2 = new ArticleGridViewAdapter(getContext(), R.layout.article_grid,
                (ArrayList<Article>) MainActivity.allArticles);
        gridView.setAdapter(adapter2);
        gridView.setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
    }

    private void onSearch(String s) {
        articles.clear();
        Hunter h = new Hunter();
        adapter2 = new ArticleGridViewAdapter(getContext(), R.layout.article_grid,
                h.searchArt((ArrayList<Article>) MainActivity.allArticles, searchView.getQuery().toString()));
        articles.addAll(h.searchArt((ArrayList<Article>) MainActivity.allArticles, searchView.getQuery().toString()));
        gridView.setAdapter(adapter2);
        gridView.setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        toneGen1.startTone(ToneGenerator.TONE_PROP_BEEP);
        //toneGen1.stopTone();
          // if ()
       /* final Thread thread = new Thread(new Runnable() {
            public void run() {
                genTone();
                handler.post(new Runnable() {

                    public void run() {
                        playSound();
                    }
                });
            }
        });
        thread.start();*/



        if (gridView.getAdapter() instanceof CategoryGridViewAdapter) {
            onCatClick(position);
        } else if (gridView.getAdapter() instanceof ArticleGridViewAdapter) {
            onArtClick(position);
            mListener.onArticleSelected(MainActivity.receiptItems);
        }
    }

    private void onArtClick(int i) {
        int pos = gridView.getFirstVisiblePosition();
        updateReceipt(i);
        for (int j = 0; j < MainActivity.receiptItems.size(); j++) {
            //System.out.println(" in " + MainActivity.receiptItems.get(j).getName() + " count: " + MainActivity.receiptItems.get(j).getCount());
        }
        adapter2 = new ArticleGridViewAdapter(getContext(), R.layout.article_grid, (ArrayList<Article>) articles);
        gridView.setAdapter(adapter2);
        gridView.setOnItemClickListener(this);
        gridView.setOnItemLongClickListener(this);
        adapter2.notifyDataSetChanged();
        gridView.smoothScrollToPosition(pos);
        gridView.setSelection(pos);
    }

    private void updateReceipt(int i) {

        int rovnake = 0;
        articles.get(i).setCount(articles.get(i).getCount() + 1);
        for (int j = 0; j < MainActivity.receiptItems.size(); j++) {
            if ((MainActivity.receiptItems.get(j).getIdentifier().equals(articles.get(i).getIdentifier()))) {
                rovnake++;
            }
        }
        if (rovnake == 0) {
            MainActivity.receiptItems.add(articles.get(i));
        }


    }

    private void onCatClick(int i) {
        articles = MainActivity.categories.get(i).getArticles();
        //System.out.println(MainActivity.categories.get(i).getArticles());
        adapter2 = new ArticleGridViewAdapter(getContext(), R.layout.article_grid, (ArrayList<Article>) articles);
        gridView.setAdapter(adapter2);
        gridView.setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
        gridView.smoothScrollToPosition(gridView.getFirstVisiblePosition());
    }

    public void deleteChosenArticles() {
        for (Article a : articles) a.setCount(0);
        adapter2 = new ArticleGridViewAdapter(getContext(), R.layout.article_grid, (ArrayList<Article>) articles);
        gridView.setAdapter(adapter2);
        gridView.setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
    }

    void genTone() {
         for (int i = 0; i < numSamples; ++i) {
            sample[i] = Math.sin(2 * Math.PI * i / (sampleRate / freqOfTone));
        }
        int idx = 0;
        for (final double dVal : sample) {
            final short val = (short) ((dVal * 32767));
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);

        }
    }

    void playSound(){
        final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, generatedSnd.length,
                AudioTrack.MODE_STATIC);
        audioTrack.write(generatedSnd, 0, generatedSnd.length);
        audioTrack.play();
    }


}

