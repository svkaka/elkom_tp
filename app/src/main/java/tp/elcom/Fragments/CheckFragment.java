package tp.elcom.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import tp.elcom.Adapters.ReceiptListViewAdapter;
import tp.elcom.Dialogs.PriceDialog;
import tp.elcom.MainActivity;
import tp.elcom.Objects.Article;
import tp.elcom.Objects.ConnectionHandler;
import tp.elcom.R;

/**
 * Created by PaFi on 16. 10. 2015.
 */
public class CheckFragment extends Fragment implements View.OnClickListener {
    ListView listView;
    Button buttonPay, buttonStorno;
    TextView textViewArt;
    TextView textViewCou;
    TextView textViewPri;
    TextView textViewTot;
    TextView totalPriceT;
    private boolean recipeOpened = false;
    private ReceiptListViewAdapter adapter;
    private ConnectionHandler connectionHandler;
    ArrayList<String> receipt = new ArrayList<>();
    private View rootView;
    private boolean readyToPay;
    private float totalPrice;
    private boolean released = false;
    private Thread thread;
    private Long delay;
    String euro = "\u20ac";
    boolean method;

    public CheckFragment() {
    }

    public static CheckFragment newInstance() {
        return new CheckFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_check, container, false);

       /* textViewArt = (TextView) rootView.findViewById(R.id.checkFragmentArt);
        textViewCou = (TextView) rootView.findViewById(R.id.checkFragmentCount);
        textViewTot = (TextView) rootView.findViewById(R.id.checkFragmentTotal);*/

        //  totalPriceT= (TextView) rootView.findViewById(R.id.suma);
        String tag = getTag();
        ((MainActivity) getActivity()).setCheckFragmentTag(tag);
        listView = (ListView) rootView.findViewById(R.id.receipt);
        buttonPay = (Button) rootView.findViewById(R.id.buttonPay);
        buttonStorno = (Button) rootView.findViewById(R.id.buttonStorno);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.receiptItems.remove(position);
                TextView textView = (TextView) view.findViewById(R.id.rcptArticleTxt);
                String text = textView.getText().toString();
                Article a = null;
                for (Article at : MainActivity.allArticles) {
                    if (at.getName().equalsIgnoreCase(text)) a = at;
                }
                MainActivity.allArticles.get(MainActivity.allArticles.indexOf(a)).setCount(0);
                updateFragment();
                String categoryFragmentTag = ((MainActivity) getActivity()).getCategoryFragmentTag();
                CategoryFragment fragment = (CategoryFragment) getFragmentManager().findFragmentByTag(categoryFragmentTag);
                fragment.updateGridView();

            }
        });
        buttonPay.setOnClickListener(this);
        buttonStorno.setOnClickListener(this);
        connectionHandler = new ConnectionHandler(getActivity());

        released = true;

        adapter = new ReceiptListViewAdapter(getContext(), R.layout.receipt_row, (ArrayList<Article>) MainActivity.receiptItems);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return rootView;
    }

    public void updateFragment() {
        updateTotalPrice();
        //totalPriceT.setText(String.format("%.2f",totalPrice));

        buttonPay.setText(getResources().getString(R.string.action_pay) + " " + String.format("%.2f" + euro, totalPrice));
        adapter = new ReceiptListViewAdapter(getContext(), R.layout.receipt_row, (ArrayList<Article>) MainActivity.receiptItems);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void updateTotalPrice() {
        totalPrice = 0;
        for (int i = 0; i < MainActivity.receiptItems.size(); i++) {
            totalPrice += (MainActivity.receiptItems.get(i).getCount() * MainActivity.receiptItems.get(i).getPrice());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonPay:
                if (getToPay() > 0) {
                    PriceDialog priceDialog = new PriceDialog();
                    method = priceDialog.getMethodChosen();
                    priceDialog.show(getFragmentManager(), "Pay");
                } else Toast.makeText(getContext(), R.string.aaaapo, Toast.LENGTH_SHORT).show();
                //pay();
                if (recipeOpened) {
                }
                break;
            case R.id.buttonStorno:
                resetReceipt();
                break;
        }
    }

    public void pay(Float f) {
        if (released) {
            beginFiscalReceipt();
            try {
                Thread.sleep(1100);
                float toPay = 0;
                for (Article a : MainActivity.receiptItems) {
                    printStock(a.getName(), String.valueOf(a.getPrice()*a.getCount()), String.valueOf(a.getCount()), "2", String.valueOf(a.getCount()), String.valueOf(a.getUnit()));
                    Thread.sleep(430);
                    toPay += a.getPrice()*a.getCount();
                }
                payReceipt(toPay, f);
                Thread.sleep(2100);
                endFiscalReceipt();
                Thread.sleep(2100);
                disconnect();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private void resetReceipt() {
        resetPrinter();
        MainActivity.receiptItems.clear();
        for (int j = 0; j < MainActivity.allArticles.size(); j++) {
            MainActivity.allArticles.get(j).setCount(0);
        }
        CategoryFragment cat = (CategoryFragment) getFragmentManager().getFragments().get(0);
        cat.deleteChosenArticles();
        updateFragment();

    }

    private void resetReceiptWithoutPrinter() {
        MainActivity.receiptItems.clear();
        for (int j = 0; j < MainActivity.allArticles.size(); j++) {
            MainActivity.allArticles.get(j).setCount(0);
        }
        CategoryFragment cat = (CategoryFragment) getFragmentManager().getFragments().get(0);
        cat.deleteChosenArticles();
        updateFragment();

    }


    private void beginFiscalReceipt() {
        if (!recipeOpened) {
            connectionHandler.setUpConnection();
            if (connectionHandler.isConnnected) {
                connectionHandler.sendCommand("CONNECT\tREQ\n", 0);
                connectionHandler.sendCommand("bFR\tREQ\t1\t\n", 0);
                recipeOpened = true;
                readyToPay = true;
            }
        }
    }

    private void printStock(String string1, String string2, String string3, String string4, String string5, String string6) {
        if (released) {
            connectionHandler.sendCommand("pRI\tREQ\t" + string1 + "\t" + string2 + "\t" + string3 + "\t" + string4 + "\t" + string5 + "\t" + string6 + "\t\t\n", 0);
        }
    }

    private void endFiscalReceipt() {
        if (released) {
            connectionHandler.sendCommand("eFR\tREQ\t1\n", 0);
            recipeOpened = false;
            readyToPay = false;
        }
    }

    private void disconnect() {
        if (released) {
            connectionHandler.sendCommand("DISCONNECT\tREQ\n", 0);
            connectionHandler.closeConnection();
        }
    }


    private void payReceipt(float sum1, float sum2) {
        if (released) {
            if (!method) {
                connectionHandler.sendCommand("pRT\tREQ\t" + sum1 + "\t" + sum2 + "\tKarta\t\t\n", 0);
                resetReceiptWithoutPrinter();
            } else
            connectionHandler.sendCommand("pRT\tREQ\t" + sum1 + "\t" + sum2 + "\tHotovosť\t\t\n", 0);
            resetReceiptWithoutPrinter();
        }
    }


    public void resetPrinter() {
        connectionHandler.setUpConnection();
        if (connectionHandler.isConnnected) {
            connectionHandler.sendCommand("CONNECT\tREQ\n", 0);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                Log.d("Jipy", e.getMessage());
            }
            connectionHandler.sendCommand("rP\tREQ\n", 0);
            connectionHandler.sendCommand("DISCONNECT\tREQ\n", 0);
            connectionHandler.closeConnection();
        }
    }

    public float getToPay() {
        updateTotalPrice();
        try {
            return totalPrice;
        } catch (NumberFormatException ex) {
            return totalPrice = 0.0f;
        }
    }

    public void setMethod(Boolean b) {
        method = b;
    }
}

