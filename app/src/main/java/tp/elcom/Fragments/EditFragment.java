package tp.elcom.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import tp.elcom.Adapters.EditArticleGridAdapter;
import tp.elcom.Adapters.EditCategoryGridAdapter;
import tp.elcom.Dialogs.CreateItemDialog;
import tp.elcom.MainActivity;
import tp.elcom.Objects.Article;
import tp.elcom.Objects.Category;
import tp.elcom.Objects.Hunter;
import tp.elcom.Objects.Sorter;
import tp.elcom.R;
import tp.elcom.ToggleButtonGroupTableLayout;

/**
 * Created by PaFi on 16. 10. 2015.
 */

public class EditFragment extends Fragment implements AdapterView.OnItemClickListener {
    private GridView gridView;
    private EditCategoryGridAdapter adapter;
    private EditArticleGridAdapter adapter2;
    private List<Article> articles = new ArrayList<>();
    private SearchView searchView;
    private List<Article> searchTemp = new ArrayList<>();
    private ImageButton sortDesc, sortAsc;
    private ArrayList mSelectedItems;

    public GridView getGridView() {
        return gridView;
    }

    public static EditFragment newInstance() {
        return new EditFragment();
    }

    public EditFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_edit, container, false);

        gridView = (GridView) rootView.findViewById(R.id.editFragmentGridView);
        adapter = new EditCategoryGridAdapter(getContext(), R.layout.category_grid_edit, (ArrayList<Category>) MainActivity.categories,getFragmentManager());
        adapter.setGridView(gridView);
        searchView = (SearchView) rootView.findViewById(R.id.searchView3);

        getGridView().setAdapter(adapter);
        adapter.notifyDataSetChanged();
        getGridView().setOnItemClickListener(this);



        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateItemDialog dialog = new CreateItemDialog();
                dialog.setGridView(gridView);
                dialog.show(getFragmentManager(), "addItem");
            }
        });

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (gridView.getAdapter() instanceof EditArticleGridAdapter) {
                        adapter.notifyDataSetChanged();
                        getGridView().setAdapter(adapter);
                        return true;
                    }
                }
                return false;
            }
        });


        //System.out.println(searchView.getQuery());
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                onSearch(s);
                return false;
            }
        });

        return rootView;
    }

    private void onSort(final String how) {
        boolean[] isSelected = {true, true,};
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setSingleChoiceItems(R.array.sortBy, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sortNow(which, how);
                builder.setCancelable(true);
            }
        });
        builder.create();
        builder.show();
    }


    private void sortNow(int which,String s) {
        Sorter sorter=new Sorter();
        sorter.sort((ArrayList) MainActivity.allArticles, s, which);
        adapter2 = new EditArticleGridAdapter(getContext(), R.layout.article_grid_edit, (ArrayList<Article>) MainActivity.allArticles,getFragmentManager());
        getGridView().setAdapter(adapter2);
        getGridView().setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
    }

    private void onSearch(String s) {
        articles.clear();
        Hunter h = new Hunter();
        adapter2 = new EditArticleGridAdapter(getContext(), R.layout.article_grid_edit, h.searchArt((ArrayList<Article>) MainActivity.allArticles, searchView.getQuery().toString()),getFragmentManager());
        articles.addAll(h.searchArt((ArrayList<Article>) MainActivity.allArticles, searchView.getQuery().toString()));
        getGridView().setAdapter(adapter2);
        getGridView().setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (getGridView().getAdapter() instanceof EditCategoryGridAdapter) {
            onCatClick(position);
        }
        //onArtClick(position);

    }

    private void onArtClick(int i) {
        int pos = gridView.getFirstVisiblePosition();
        for (int j = 0; j < MainActivity.receiptItems.size(); j++) {
            //System.out.println(" in " + MainActivity.receiptItems.get(j).getName() + " count: " + MainActivity.receiptItems.get(j).getCount());
        }
        adapter2 = new EditArticleGridAdapter(getContext(), R.layout.article_grid_edit, (ArrayList<Article>) articles,getFragmentManager());
        adapter2.setGridView(gridView);
        getGridView().setAdapter(adapter2);
        getGridView().setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
        gridView.smoothScrollToPosition(pos);
        gridView.setSelection(pos);
    }

    private void onCatClick(int i) {
        //System.out.println("onCat "+i);
        articles = MainActivity.categories.get(i).getArticles();
        //System.out.println(MainActivity.categories.get(i).getArticles());
        adapter2 = new EditArticleGridAdapter(getContext(), R.layout.article_grid_edit, (ArrayList<Article>) articles,getFragmentManager());
        //if(i == 0) adapter2.setIsInAll(true);
        //else adapter2.setIsInAll(false);
        //adapter2.setGridView(gridView);
        getGridView().setAdapter(adapter2);
        getGridView().setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
    }

   /* public void deleteChosenArticles() {
        for (Article a : articles) a.setCount(0);
        adapter2 = new ArticleGridViewAdapter(getContext(), R.layout.article_grid_edit, (ArrayList<Article>) articles);
        gridView.setAdapter(adapter2);
        gridView.setOnItemClickListener(this);
        adapter2.notifyDataSetChanged();
    }*/
}


