package tp.elcom;

/**
 * Created by PaFi on 2. 11. 2015.
 */

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import tp.elcom.Objects.Article;


public class ImportActivity extends Activity implements View.OnClickListener {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_READ = 1;
    private String fileName;
    private TextView currentPath;
    private List<String> itemList = null;
    private List<String> pathList = null;
    private List<String> fileNames = null;
    private List<String> pathes = null;

    private int count =0;

  private  ListView diskLV = null;
  private  ListView selectedLV = null;
    private String root;
    private String prew;
    private Button button;

    private String selected;
    private String value;
    private String shortName;
    private String dirPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        value = intent.getStringExtra("key");

        dirPath = String.valueOf(getFilesDir());
        setContentView(R.layout.no_idea);
        diskLV = (ListView) findViewById(R.id.list);
        selectedLV = (ListView) findViewById(R.id.list2);
        fileNames = new ArrayList<>();
        pathes = new ArrayList<>();
        root = Environment.getExternalStorageDirectory().getPath();
        selected = "";
        currentPath = (TextView) findViewById(R.id.selection);
        button = (Button) findViewById(R.id.button);


        if (value.equals("i")) button.setText("Import");
        if (value.equals("e")) button.setText("Export");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButton();
            }
        });
        diskLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onDiskedClick(position);
            }
        });
        selectedLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onSelectedClick(position);
            }
        });
        getDir(root);
    }

    public String getPath() {
        return String.valueOf(getFilesDir());
    }

    private void onSelectedClick(int position) {
        fileNames.remove(position);
        pathes.remove(position);
        selectedLV.setAdapter(new ArrayAdapter<String>(this, R.layout.krowka, fileNames));
    }

    private void getDir(String dirPath) {
        currentPath.setText("Location: " + dirPath);

        itemList = new ArrayList<String>();
        pathList = new ArrayList<String>();

        File f = new File(dirPath);
        File[] files = f.listFiles();

        if (!dirPath.equals(root)) {
            itemList.add(root);
            pathList.add(root);
            itemList.add("../");
            pathList.add(f.getParent());
            prew = f.getParent();
        }
        if (files != null)
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                pathList.add(file.getPath());
                if (file.isDirectory() && !file.isHidden() && file.canRead()) {
                    itemList.add(file.getName() + "/");
                } else {
                    itemList.add(file.getName());
                }
            }

        diskLV.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, itemList));
    }

    protected void onDiskedClick(int position) {
        File file = new File(pathList.get(position));
        if (file.isDirectory()) {
            if (file.canRead() && !file.isHidden()) {
                getDir(pathList.get(position));
            } else {
                new AlertDialog.Builder(this)
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle("[" + file.getName() + getString(R.string.cantreadfolder))
                        .setPositiveButton("OK", null).show();
            }
        } else {
            if (file.getName().endsWith(".txt")) {
                fileNames.add(" x    " + file.getName());
                pathes.add(file.getAbsolutePath());
                shortName = file.getName();
                selected += (file.getAbsolutePath() + "\n");
                selectedLV.setAdapter(new ArrayAdapter<>(this, R.layout.krowka, fileNames));
            } else openFile(this, file);
        }
    }

    protected String thisIsIt() {
        String s = "";
        for (int i = 0; i < pathes.size(); i++) {
            s += pathes.get(i) + "\n";
        }
        return s;
    }

    public void onButton() {
        if (value.equals("i")) {
            Intent myIntent = new Intent(ImportActivity.this, MainActivity.class);
            myIntent.putExtra("key", thisIsIt());
            this.startActivity(myIntent);
            //startMain();
        }
        if (value.equals("e")) {
            dialogSave();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    public static void openFile(final Context context, final File target) {
        final Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType(Uri.fromFile(target), "*/*");
        if (context.getPackageManager().queryIntentActivities(i, 0).isEmpty()) {
            return;
        }
        try {
            context.startActivity(i);
        } catch (Exception e) {
        }
    }

    @Override
    public void onBackPressed() {

        System.out.println("|" + prew + "|" + root + "|");
        if (prew != null) getDir(prew);
        else super.onBackPressed();
        if (prew == null || prew.equals(root)){
             if (count==1)
            super.onBackPressed();
            count++;
        }
    }

    @Override
    public void onClick(View v) {
        Intent myIntent = new Intent(ImportActivity.this, MainActivity.class);
        myIntent.putExtra("key", selected);
        this.startActivity(myIntent);
    }
//Todo upravit poriadne.
    private void dialogSave() {

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_save_file, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextSaveFile);
        alertDialogBuilder
                .setTitle(R.string.svfiledialog)
                .setCancelable(false)
                .setPositiveButton(R.string.savefile,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                fileName = userInput.getText().toString();
                                Writer writer = null;
                                try {
                                    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName), "utf-8"));
                                    writer.write(prepareForOutput());
                                    writer.close();
                                    Toast.makeText(getApplicationContext(), getString(R.string.savefiletoast) + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName, Toast.LENGTH_SHORT).show();
                                } catch (UnsupportedEncodingException e) {
                                    Toast.makeText(getApplicationContext(), R.string.filenotsaved, Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                } catch (FileNotFoundException e) {
                                    Toast.makeText(getApplicationContext(), R.string.filenotsaved, Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    Toast.makeText(getApplicationContext(), R.string.filenotsaved, Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                                startMain();
                            }
                        }
                )
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }
                );
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void startMain() {
        Intent myIntent = new Intent(ImportActivity.this, MainActivity.class);
        this.startActivity(myIntent);
    }

    private String prepareForOutput() {
        String s = "";
        for (int i = 0; i < MainActivity.allArticles.size(); i++) {
            Article a = MainActivity.allArticles.get(i);
            s += MainActivity.allArticles.get(i).getName() + ";";
            s += MainActivity.allArticles.get(i).getPrice() + ";";
            s += MainActivity.allArticles.get(i).getUnit() + ";";
            s += MainActivity.allArticles.get(i).getCategory() + ";";
            s += MainActivity.allArticles.get(i).getIdentifier() + ";;\n";
        }
        //System.out.println(s);
        return s;
    }
}


