package tp.elcom;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import tp.elcom.Adapters.SectionsPagerAdapter;
import tp.elcom.Fragments.CategoryFragment;
import tp.elcom.Notification.NotificationEventReceiver;
import tp.elcom.Objects.Article;
import tp.elcom.Objects.Category;
import tp.elcom.Objects.ConnectionHandler;
import tp.elcom.Objects.DataSource;
import tp.elcom.Objects.Extractor;
import tp.elcom.Objects.Verifier;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, TabLayout.OnTabSelectedListener, CategoryFragment.OnArticleSelectedListener {

    private static final int MY_PERMISSIONS_REQUEST_WRITE_READ = 21;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    public static List<Article> allArticles;
    public static List<Category> categories;
    public static List<Article> receiptItems;
    public static DataSource dataSource;
    public HashMap<String, Integer> map = new HashMap<>();
    private String value;
    private String categoryFragmentTag, checkFragmentTag;
    private Category cat;
    private ConnectionHandler connectionHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Intent intent = getIntent();
        value = intent.getStringExtra("key");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        connectionHandler = new ConnectionHandler(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        dataSource=new DataSource(this);
        dataSource.open();
        // Create list of tabs
        ArrayList<String> tabs = new ArrayList<>();
        tabs.add(getString(R.string.tab1));
        tabs.add(getString(R.string.tab2));
        tabs.add(getString(R.string.tab3));

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mSectionsPagerAdapter.setTabs(tabs);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Set up tabLayout with adapter and listener
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabsFromPagerAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(this);

         allArticles = new ArrayList<>();
         categories = new ArrayList<>();
         receiptItems = new ArrayList<>();

        cat = new Category(getString(R.string.allArticle), "#e3e3e3");
        cat.setIdentifier(0);
        categories.add(cat);

        allArticles.addAll(dataSource.getAllArticles());
        categories.addAll(dataSource.getAllCategories());
        for (int i = 0; i < MainActivity.allArticles.size(); i++) {
            for (int j = 0; j < MainActivity.categories.size(); j++) {
                if(MainActivity.allArticles.get(i).getCategory().equals(""+MainActivity.categories.get(j).getIdentifier())) {
                    MainActivity.allArticles.get(i).setCategory(MainActivity.categories.get(j).getName());
                }
            }
            }

        Verifier v =new Verifier();
        v.assignCategories();

       //if import activity send sth
        value = this.getIntent().getStringExtra("key");
        if (value!=null) {
            try {
                letsHaveFun(value.replace("\n",""));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
                super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_import:
                requestPermission();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.formMessage)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startImportActivity("i");
                            }
                        });
                builder.create();
                builder.show();

                break;
            case R.id.nav_export:
                requestPermission();
                startImportActivity("e");

                break;
            case R.id.nav_dbsDel:
                dbsClear();
                break;
            case R.id.nav_uzavierka:
                printDailyReport();
                break;
            /*case R.id.nav_settings:
                Intent myIntent = new Intent(MainActivity.this, SettingsActivity.class);
                //myIntent.putExtra("key", s);
                this.startActivity(myIntent);
                break;*/
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void dbsClear() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.cleardbs)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearAppData();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void startImportActivity(String s) {

        Intent myIntent = new Intent(MainActivity.this, ImportActivity.class);
        myIntent.putExtra("key", s);
        this.startActivity(myIntent);
    }

    private void letsHaveFun(String s) throws FileNotFoundException {
        Extractor extractor = new Extractor(new File(s));
        allArticles.addAll(extractor.extractArticles());
        Verifier v = new Verifier();
        v.assignCategories();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void clearAppData() {
        if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
            ((ActivityManager)this.getSystemService(ACTIVITY_SERVICE))
                    .clearApplicationUserData(); // note: it has a return value!
        } else {
            try {
            // clearing app data
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear tp.elcom");

        } catch (Exception e) {
            e.printStackTrace();
        }
            // use old hacky way, which can be removed
            // once minSdkVersion goes above 19 in a few years.
        }


    }

    @Override
    public void onArticleSelected(List<Article> a) {
        //android.app.FragmentManager manager = getFragmentManager();
        //android.app.Fragment f =  manager.findFragmentById(R.layout.fragment_check);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
        if (mSectionsPagerAdapter.getF() != null) mSectionsPagerAdapter.getF().updateFragment();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        //  fragmentTransaction.hide(getFragmentManager().getFragment(m.view,"Key"));
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    protected void onPause() {
        super.onPause();

        File file = new File(getApplicationContext().getFilesDir(), "Receipt");
        HashMap<String, Integer> outputMap = new HashMap<>();
        for (int i = 0; i < receiptItems.size(); i++) {
            Article a = receiptItems.get(i);
            outputMap.put(a.getName(), a.getCount());
        }
        //System.out.println("HASH: " + outputMap);
        FileOutputStream fos;
        try {
            fos = openFileOutput(file.getName(), Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(outputMap);
            oos.flush();
            oos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println("File saved.");
    }

    private void updateReceiptItems() {
        FileInputStream fis;
        try {
            fis = openFileInput("Receipt");
            //System.out.println("File accessed.");
            ObjectInputStream ois = new ObjectInputStream(fis);
            HashMap<String, Integer> inputMap = (HashMap) ois.readObject();
            map = inputMap;
            //System.out.println("HASH updated: " + map);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        receiptItems.clear();
        for (Article a : allArticles) {
            if (map.containsKey(a.getName())) {
                a.setCount(map.get(a.getName()));
                receiptItems.add(a);
            }

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateReceiptItems();
    }

    private void requestPermission(){
        if(Build.VERSION.SDK_INT>=23) {
            String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
            String permission2 = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_READ);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_READ: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public static Boolean checkPermissions(Activity activity) {
        Boolean res = false;
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_READ);
        } else {
            res = true;
        }

        return res;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void createNotification() {

        PendingIntent pendingIntent;
        Intent myIntent = new Intent(MainActivity.this, NotificationEventReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent, 0);

        AlarmManager am= (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        calendar.set(Calendar.MINUTE, 56);
        am.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        //System.out.println(calendar.getTimeInMillis()+"CAS na budik" + System.currentTimeMillis()+" " +calendar.getTime());
      /*  Intent intent = new Intent(MainActivity.this, NotifyService.class);
        MainActivity.this.startService(intent);
*/

// toto ne
/*.setDefaults(Notification.DEFAULT_VIBRATE)
                .setDefaults(Notification.PRIORITY_MAX)
                .setDefaults(Notification.VISIBILITY_PUBLIC)
                               .setSound(soundUri)
                               */
   // NotificationEventReceiver.setupAlarm(getApplicationContext());
         /*
        Intent intent = new Intent(this, MainActivity.class);
        //Intent intent = new Intent(this, NotificationReceiverActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        long[] vibrate = { 0, 100, 200, 300 };
        Notification n = new Notification.Builder(this)



        //tu oddelene
               .setContentTitle("New Post!")
                .setContentText("Here's an awesome update for you!")
                .setSmallIcon(R.mipmap.ic_uzav)
                .setContentIntent(pIntent)

                .addAction(R.mipmap.ic_uzav, "View", pIntent)
                .build();
n.sound=Uri.parse("android.resource://"
        + this.getPackageName() + "/" + R.raw.noot);
        n.vibrate = vibrate;

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, n);
*/

    }

    public String getCategoryFragmentTag() {
        return categoryFragmentTag;
    }

    public void setCategoryFragmentTag(String categoryFragmentTag) {
        this.categoryFragmentTag = categoryFragmentTag;
    }

    public void setCheckFragmentTag(String tag) {
        this.checkFragmentTag = tag;
    }

    public String getCheckFragmentTag() {
        return checkFragmentTag;
    }

    public void printDailyReport() {
        connectionHandler.setUpConnection();
        if (connectionHandler.isConnnected) {
            connectionHandler.sendCommand("CONNECT\tREQ\n", 0);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {

            }
            connectionHandler.sendCommand("pZR\tREQ\n", 0);
            connectionHandler.sendCommand("DISCONNECT\tREQ\n", 0);
            connectionHandler.closeConnection();
        }
    }
}
