package tp.elcom.Objects;

import java.util.Formatter;

/**
 * Created by PaFi on 16. 10. 2015.
 */
public class Article {
    private String name=null;
    private String category=null;;
    private float price=0;;
    private String color=null;;
    private int count = 0;
    private String identifier=null;;
    private String unit=null;;
    private int dbsID=0;

    public Article() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getDbsID() {
        return dbsID;
    }

    public void setDbsID(int dbsID) {
        this.dbsID = dbsID;
    }

    @Override
    public String toString() {
        Formatter f = new Formatter();
        f.format("Name: %s, Category: %s, Price: %f, Colour: %s, Count: %d, Id: %s, Unit: %s DBS-Id: %d  \n",
                this.getName(), this.getCategory(), this.getPrice() ,this.getColor(), this.getCount(),this.getIdentifier(),this.getUnit(),this.getDbsID()
        );
        return f.toString();
    }


}


