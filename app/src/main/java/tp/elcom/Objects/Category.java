package tp.elcom.Objects;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

import tp.elcom.MainActivity;
import tp.elcom.R;

/**
 * Created by PaFi on 16. 10. 2015.
 */
public class Category {
    private List<Article> articles = new ArrayList<>();
    private String name;
    private String color;
    private int identifier;

    public Category(String s1, String s2){
        this.name=s1;
        this.color=s2;

    }
    public Category() {
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void addItem(Article a) {
        //System.out.println(a.getName());
        articles.add(a);
        a.setColor(color);
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    @Override
    public String toString() {
        Formatter f = new Formatter();
        f.format("Name: %s, Color: %s, DBS-ID: %d  \n",
                this.getName(), this.getColor(), this.getIdentifier()
        );
        return f.toString();
    }
}
