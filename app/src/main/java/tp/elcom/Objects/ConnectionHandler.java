package tp.elcom.Objects;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.usb.UsbManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import tp.elcom.R;

/**
 * Created by mikita on 8. 9. 2015.
 */
public class ConnectionHandler {

    private Activity activity;
    private UsbManager manager;
    private SerialInputOutputManager serialInputOutputManager;
    private UsbSerialDriver usbSerialDriver;
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();
    public boolean isConnnected;

    private final String TAG = ConnectionHandler.class.getSimpleName();

    private final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {

                @Override
                public void onRunError(Exception e) {
                    Log.d(TAG, "Runner stopped.");
                }

                @Override
                public void onNewData(final byte[] data) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ConnectionHandler.this.updateReceivedData(data);
                        }
                    });
                }
            };


    public ConnectionHandler(Activity activity) {
        this.activity = activity;
        manager = (UsbManager) activity.getSystemService(Context.USB_SERVICE);
    }


    public void setUpConnection() {

        usbSerialDriver = UsbSerialProber.acquire(manager);

        if (usbSerialDriver == null) {
            isConnnected = false;
            makeErrorDialog();
        } else {
            isConnnected = true;
            Log.d(TAG, "Resumed port");
            try {
                usbSerialDriver.open();
            } catch (IOException e) {
                Log.e(TAG, "Can't open port: " + e.getMessage());
            }
            startIoManager();
        }

    }

    public void closeConnection() {

            stopIoManager();
            try {
                usbSerialDriver.close();
            } catch (Exception e) {
                Log.e(TAG, "Error closing device or device not connected: " + e.getMessage());
            }

    }


    private void stopIoManager() {
        if (serialInputOutputManager != null) {
            Log.i(TAG, "Stopping io manager ..");
            serialInputOutputManager.stop();
            serialInputOutputManager = null;
        }
    }

    private void startIoManager() {
        if (usbSerialDriver != null) {
            Log.i(TAG, "Starting io manager ..");
            serialInputOutputManager = new SerialInputOutputManager(usbSerialDriver, mListener);
            mExecutor.submit(serialInputOutputManager);
        }
    }

    private void updateReceivedData(byte[] data) {
        final String message = "Read " + data.length + " bytes: \n"
                + HexDump.dumpHexString(data) + "\n\n";
        Log.i(TAG, "Received data: " + message);
    }

    public void sendCommand(String command, int timeout) {
            try {
                byte[] mess = (command).getBytes("Cp1250");
                usbSerialDriver.write(mess, timeout);
            } catch (Exception e) {
                Log.i(TAG, "Error sending data or device not connected: " + e.getMessage());
            }
    }

    public void makeErrorDialog() {
        AlertDialog dialog = new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.deviceconnect))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


}
