package tp.elcom.Objects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PaFi on 20. 11. 2015.
 */
public class DataSource {
    private SQLiteDatabase db;
    private Database dbHelper;

    public DataSource(Context context) {
        dbHelper = new Database(context);
    }
    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    private Article cursorToArticle(Cursor cursor) {
        Article article = new Article();

        if (cursor.getCount() > 0) {
            article.setDbsID(cursor.getInt(0));
            article.setName(cursor.getString(1));
            article.setPrice(cursor.getFloat(2));
            article.setUnit(cursor.getString(3));
            article.setIdentifier(cursor.getString(4));
            article.setCategory(String.valueOf(cursor.getInt(5)));
        }
        //System.out.println(article);
        return article;
    }

    private Category cursorToCategory(Cursor cursor) {
        Category category = new Category();
        if (cursor.getCount() > 0) {
            category.setIdentifier(cursor.getInt(0));
            category.setName(cursor.getString(1));
            category.setColor(cursor.getString(2));
        }
        //System.out.println(category);
        return category;
    }

    public List<Article> getAllArticles() {
        List<Article> articleList = new ArrayList<>();
        Cursor cursor = db.query(Database.TABLE_ARTICLE,
                Database.articleColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            articleList.add(cursorToArticle(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return articleList;
    }

    public List<Category> getAllCategories() {
        List<Category> categoryList = new ArrayList<>();
        Cursor cursor = db.query(Database.TABLE_CATEGORY,
                Database.categoryColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            categoryList.add(cursorToCategory(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return categoryList;
    }

    public void insertCategory(ContentValues values) {
       /* ContentValues values=new ContentValues();
        values.put(Database.CATEGORY_NAME, comment[1]);
        values.put(Database.CATEGORY_COLOUR, comment[2]);*/
        db.insert(Database.TABLE_CATEGORY, null, values);
    }

    public void insertArticle(ContentValues values){
        db.insert(Database.TABLE_ARTICLE, null, values);
    }

    public void deleteArticle(Article article) {
        db.delete(Database.TABLE_ARTICLE, Database.ARTICLE_ID
                + " = " + article.getDbsID(), null);
    }

    public void deleteCategory(Category category) {
        db.delete(Database.TABLE_CATEGORY, Database.CATEGORY_ID
                + " = " + category.getIdentifier(), null);
    }


    //rec: dataSource.updateArticle(args, Database.ARTICLE_ID + " = " + article.getDbs);
    public void updateArticle(ContentValues values, String filter){
        db.update(Database.TABLE_ARTICLE, values, filter, null);
    }

    public void updateCategory(ContentValues values, String filter){
        db.update(Database.TABLE_CATEGORY, values, filter, null);
    }

    public void dropTable(){
        db.execSQL("DROP TABLE IF EXISTS " + Database.TABLE_ARTICLE);
        db.execSQL("DROP TABLE IF EXISTS " + Database.TABLE_CATEGORY);
    dbHelper.onCreate(db);
}
    public int getCatId(String name) {
        String id = null;
        Cursor cursor = db.rawQuery("SELECT "+Database.CATEGORY_ID+" FROM "+Database.TABLE_CATEGORY+" WHERE "+Database.CATEGORY_NAME+"= '"+name.trim()+"'", null);
        if (cursor.moveToFirst()){
            id = cursor.getString(cursor.getColumnIndex(Database.CATEGORY_ID));
        }
        cursor.close();
        return Integer.parseInt(id);
    }
}
