package tp.elcom.Objects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PaFi on 20. 11. 2015.
 */
public class Database extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 3;
    public static final String TABLE_ARTICLE = "table_article";
    public static final String ARTICLE_ID = "article_id";
    public static final String ARTICLE_NAME = "article_name";
    public static final String ARTICLE_PRICE = "article_price";
    public static final String ARTICLE_UNIT = "article_unit";
    public static final String ARTICLE_IDENTIFIER = "article_identifier";
    public static final String ARTICLE_CAT = "article_reference";
    public static final String TABLE_CATEGORY = "table_category";
    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_COLOUR = "category_colour";
    public  static final String DATABASE_NAME = "dbs";
    public  static final String CREATE_TABLE_ARTICLE =
            "create table " + TABLE_ARTICLE + "(" + ARTICLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                    + ARTICLE_NAME + " TEXT, " + ARTICLE_PRICE + " REAL, " + ARTICLE_UNIT + " TEXT, "
                    + ARTICLE_IDENTIFIER + " TEXT, " + ARTICLE_CAT + " INTEGER,"
                    + " FOREIGN KEY(" + ARTICLE_CAT + ") REFERENCES "
                    + TABLE_CATEGORY + " (" + CATEGORY_ID + "));";

    private static final String CREATE_TABLE_CATEGORY =
            "create table " + TABLE_CATEGORY + "(" + CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                    + CATEGORY_NAME + " TEXT, " + CATEGORY_COLOUR + " TEXT);";

    public static String[] articleColumns = {ARTICLE_ID,
            ARTICLE_NAME,
            ARTICLE_PRICE,
            ARTICLE_UNIT,
            ARTICLE_IDENTIFIER,
            ARTICLE_CAT};
    public static String[] categoryColumns = {CATEGORY_ID,
            CATEGORY_NAME,
            CATEGORY_COLOUR};
    private SQLiteDatabase dbs;

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CATEGORY);
        db.execSQL(CREATE_TABLE_ARTICLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        onCreate(db);
    }


}
