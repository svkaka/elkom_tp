package tp.elcom.Objects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tp.elcom.MainActivity;
import tp.elcom.Objects.Verifier;

/**
 * Created by PaFi on 2. 11. 2015.
 */
public class Extractor {
    /**
     * Supported form name + category + price + id;
     * example rožok + pečivo + 0.08 + 34 321 688 6;
     */

    private File file;
    //private Pattern getAll = Pattern.compile("(.*?)(\\+)(.*?)(\\+)(.*?)(\\+)(.*?)(;)");
    private Pattern getAll = Pattern.compile("(.*)(;)(.*)(;)(.*)(;)(.*)(;)(.*)(;;)");
    private String name;
    private String category;
    private String id;
    private float price;
    String s = "";


    public Extractor(File f) {
        file = f;
    }

    public List<Article> extractArticles() throws FileNotFoundException {
        System.out.println("EXTRACT");
        List<Article>articles=new ArrayList<>();
        InputStream stream = new FileInputStream(file.getAbsoluteFile());
        Scanner scanner = new Scanner(stream);
        while (scanner.hasNextLine()) {
            s=scanner.nextLine()+"\n";
            System.out.println(s);
            System.out.println(MainActivity.allArticles);
            Matcher m;
                m = getAll.matcher(s);
                m.matches();
            if (m.find()) {
                //System.out.println(m.group(1) + " NAME " + m.group(3) + " PRICE " + m.group(5) + " UNIT " + m.group(7) + " CATEGORY " + m.group(9) + " IDENTIFIER ");
                Verifier verifier =new Verifier();
                verifier.verifyNameAndIdentifier(m.group(1), m.group(9));
                verifier.verifyCategory(m.group(7));
                verifier.verifyPrice(m.group(3).replace(",", "."));
                verifier.verifyUnit(m.group(5));
                verifier.verifyContent();
                //verifier.verifyPrice(Float.valueOf(m.group(3).replace(",", ".")));

                /*if(verifier.verifyContent(){
                MainActivity.allArticles.add((Article)verifier.verifyContent());}*/
            }
        }
        System.out.println("In extract"+articles);
        return articles;
    }

}
