package tp.elcom.Objects;

import java.util.ArrayList;

/**
 * Created by PaFi on 2. 11. 2015.
 */
public class Hunter{
    public ArrayList searchArt(ArrayList<Article> arrayList, String s) {
        ArrayList tempList = new ArrayList();
        for (Article a : arrayList) {
            if (a.getName().toLowerCase().matches(s.toLowerCase() + ".*")||a.getName().toUpperCase().matches(s.toUpperCase()+ ".*")) {
                tempList.add(a);
            }
        }
        return tempList;
    }
}
