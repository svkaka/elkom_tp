package tp.elcom.Objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tp.elcom.MainActivity;

/**
 * Created by PaFi on 1. 11. 2015.
 */
public class Sorter {

    public Sorter() {
    }

    public List sort(ArrayList arrayList, String order, int what) {
        switch (order) {
            case "d":
                switch (what){
                    case 0:
                        Collections.sort(arrayList, new NameComparator());
                        Collections.reverse(arrayList);
                        break;
                    case 1:
                        Collections.sort(arrayList, new PriceComparator());
                        break;
                }
                break;

            case "a":
                switch (what){
                    case 0:
                        Collections.sort(arrayList, new NameComparator());
                        break;
                    case 1:
                        Collections.sort(arrayList, new PriceComparator());
                        Collections.reverse(arrayList);
                        break;

                }
                break;
        }
        return arrayList;
    }


    class NameComparator implements Comparator<Article> {
        @Override
        public int compare(Article lhs, Article rhs) {
            return lhs.getName().compareToIgnoreCase(rhs.getName());
        }
            }


    class PriceComparator implements Comparator<Article> {
        @Override
        public int compare(Article a, Article b) {
            return a.getPrice() < b.getPrice() ? -1 : a.getPrice() == b.getPrice() ? 0 : 1;
        }
    }

}
