package tp.elcom.Objects;

import android.content.ContentValues;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tp.elcom.MainActivity;

/**
 * Created by PaFi on 20. 11. 2015.
 */
public class Verifier {
    private Article article;
    private Category category;
    private boolean yeses = true;
    private String name, price, unit, cat, colour;
    String barCode = "";

    public Verifier() {
        article = new Article();
    }

    public Verifier(String barCode, String name, String price, String unit, String category) {
        this.article = new Article();
        this.barCode = barCode;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.cat = category;
    }

    public Verifier(String name, String colour) {
        this.category = new Category();
        this.name = name;
        this.colour = colour;
    }

    public Object verify() {
        if(this.article != null) {
            verifyNameAndIdentifier(name, barCode);
            if (yeses) {
                verifyCategory(cat);
                if (yeses) {
                    verifyPrice(price);
                    if (yeses) {
                        verifyUnit(unit);
                        if(yeses) return this.article;
                    }
                }
            }
        }
        else if(this.category != null) {
            verifyNameAndIdentifier(name, String.valueOf(MainActivity.categories.size() + 1));
            if (yeses) {
                verifyColor(colour);
                if (yeses) return this.category;
            }
        }
        return null;
    }

    public void verifyNameAndIdentifier(String s, String s2) {
        if ((s.length() < 1)) {
            yeses = false;
            return;
        }
        if(this.article != null) {
            for (int i = 0; i < MainActivity.allArticles.size(); i++) {
                if ((MainActivity.allArticles.get(i).getName().equals(s))) {
                    yeses = false;
                    return;
                }
            }
            this.article.setName(s);
            this.article.setIdentifier(s2);
        }
        else if(this.category != null) {
            for (int i = 0; i < MainActivity.categories.size(); i++) {
                if ((MainActivity.categories.get(i).getName().equals(s)) ||
                        (MainActivity.categories.get(i).getIdentifier() == (Integer.valueOf(s2)))) {
                    yeses = false;
                    return;
                }
            }
            this.category.setName(s);
            this.category.setIdentifier(Integer.parseInt(s2));
        }
    }

    public void verifyCategory(String s) {
        if (yeses) {
            for (int i = 0; i < MainActivity.categories.size(); i++) {
                if (MainActivity.categories.get(i).getName().equals(s)) {
                    this.article.setCategory(s);
                    return;
                }
            }
            String colour = getColor();
            Category c = new Category(s, colour);
            c.addItem(this.article);
            ContentValues values = new ContentValues();
            values.put(Database.CATEGORY_NAME, c.getName());
            values.put(Database.CATEGORY_COLOUR, c.getColor());
            MainActivity.dataSource.insertCategory(values);
            c.setIdentifier(MainActivity.dataSource.getCatId(c.getName()));
            MainActivity.categories.add(c);
            this.article.setCategory(s);
        }
    }

    public void verifyPrice(String s) {
        Float f;
        if (yeses) {
            if (!s.equals("")) {
                f = Float.parseFloat(s);
                if (f < 0) {
                    yeses = false;
                    return;
                } else article.setPrice(f);
            } else yeses = false;
        }
    }

    public void verifyColor(String s) {

        String color;
        switch(s) {
            case "Red":
                color = "#BD3632";
                break;
            case "Yellow":
                color = "#FFFF00";
                break;
            case "Blue":
                color = "#1F807F";
                break;
            case "Green":
                color = "#3D9441";
                break;
            default:
            color = getColor();
                break;
        }
        category.setColor(color);
    }

    public void verifyUnit(String s) {
        if (yeses) {
            Pattern p = Pattern.compile("(ks|g)");
            Matcher m = p.matcher(s.toLowerCase().trim());
            if(m.matches()) article.setUnit(s);
        }
    }

    public void verifyContent() {
        if (yeses) {
            if (this.article!=null) {
                     Category cat = null;
                for(int j = 0; j < MainActivity.categories.size(); j++) {
                        if(MainActivity.categories.get(j).getName().equals(article.getCategory())) {
                            cat = MainActivity.categories.get(j);
                            break;
                        }
                    }
                    ContentValues values=new ContentValues();
                    values.put(Database.ARTICLE_NAME, article.getName());
                    values.put(Database.ARTICLE_PRICE, article.getPrice());
                    values.put(Database.ARTICLE_UNIT, article.getUnit());
                    values.put(Database.ARTICLE_IDENTIFIER, article.getIdentifier());
                    values.put(Database.ARTICLE_CAT, cat.getIdentifier());
                    
                    MainActivity.dataSource.insertArticle(values);
                    MainActivity.allArticles.add(article);
            }
            else if(this.category!=null) {
                    ContentValues values=new ContentValues();
                    values.put(Database.CATEGORY_NAME, category.getName());
                    values.put(Database.CATEGORY_COLOUR,category.getColor());

                    MainActivity.dataSource.insertCategory(values);
                    MainActivity.categories.add(category);
            }
        } 
        
    }

    public String getColor(){
        ColourSet s=ColourSet.getRandom();
        for (Category c :MainActivity.categories){
            if((c.getColor()).equals(s)) {
                getColor();
            }
        }
        return "#"+s;
    }

    public void assignCategories() {
        for (int i = 0; i < MainActivity.allArticles.size(); i++) {
            for (int j = 0; j < MainActivity.categories.size(); j++) {
                if (MainActivity.allArticles.get(i).getCategory().equals(MainActivity.categories.get(j).getName())) {
                    if (!(MainActivity.categories.get(j).getArticles().contains(MainActivity.allArticles.get(i)))) {
                        MainActivity.categories.get(j).addItem(MainActivity.allArticles.get(i));
                        }
                    MainActivity.allArticles.get(i).setColor(MainActivity.categories.get(j).getColor());

                } else if (MainActivity.categories.get(j).getIdentifier() == 0) {
                    if (!MainActivity.categories.get(0).getArticles().contains(MainActivity.allArticles.get(i))) {
                        MainActivity.categories.get(0).addItem(MainActivity.allArticles.get(i));
                    }
                }
            }
        }
    }

}
